import type { NextApiRequest, NextApiResponse } from 'next';
import { todos } from "./data"; 
import type { Todo } from './data';


type Data = {
    name: string;
    todos: Array<Todo>;
};

export default function handler(req: NextApiRequest, res: NextApiResponse<Data>) {
  res.status(200).json({ name: 'John Doe', todos});
}
